FROM mcr.microsoft.com/dotnet/framework/sdk:4.8-windowsservercore-ltsc2019 AS builder
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# add SSDT build tools
RUN nuget install Microsoft.Data.Tools.Msbuild -Version 10.0.61804.210

# add SqlPackage tool
ENV download_url="https://download.microsoft.com/download/6/E/4/6E406E38-0A01-4DD1-B85E-6CA7CF79C8F7/EN/x64/DacFramework.msi"
RUN Invoke-WebRequest -Uri $env:download_url -OutFile DacFramework.msi ; \
    Start-Process msiexec.exe -ArgumentList '/i', 'DacFramework.msi', '/quiet', '/norestart' -NoNewWindow -Wait; \
    Remove-Item -Force DacFramework.msi

COPY /AwesomeDB .
RUN msbuild AwesomeDB.sqlproj \
    /p:SQLDBExtensionsRefPath="C:\Microsoft.Data.Tools.Msbuild.10.0.61804.210\lib\net46" \
    /p:SqlServerRedistPath="C:\Microsoft.Data.Tools.Msbuild.10.0.61804.210\lib\net46"

FROM octopusdeploy/mssql-server-windows-express:latest
# Install SQLPackage for Linux and make it executable
RUN Invoke-WebRequest -Uri https://go.microsoft.com/fwlink/?linkid=2157302 -OutFile sqlpackage.zip; Expand-Archive -Path sqlpackage.zip -DestinationPath sqlpackage

# Add the DACPAC to the image
COPY --from=builder ["C:/bin/Debug/AwesomeDB.dacpac", "/db.dacpac"]

# Configure external build arguments to allow configurability.
ARG DBNAME=AwesomeDB
ARG PASSWORD

# Configure the required environmental variables
ENV ACCEPT_EULA=Y
ENV SA_PASSWORD=$env:PASSWORD
RUN "C:\sqlpackage\sqlpackage.exe" /a:Publish /sf:"db.dacpac" /tsn:. /tdn:$env:DBNAME