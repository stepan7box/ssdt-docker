﻿CREATE TABLE [dbo].[UserRoles]
(
	[UserRoleId] INT NOT NULL PRIMARY KEY,
	[RoleName] NVARCHAR(50) NOT NULL
)
