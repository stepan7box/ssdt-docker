﻿CREATE TABLE [dbo].[Users]
(
	[UserId] INT NOT NULL PRIMARY KEY IDENTITY,
	[UserRoleId] INT NOT NULL,
	[FirstName] NVARCHAR NOT NULL,
	[LastName] NVARCHAR NOT NULL, 
	CONSTRAINT [FK_Users_UserRoles_UserRoleId] FOREIGN KEY ([UserRoleId]) REFERENCES [UserRoles]([UserRoleId])
)
